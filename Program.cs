﻿using ConnectDB.Entities;
using System;
using System.ComponentModel;
using System.Linq;

namespace ConnectDB
{
    class Program
    {/*
        //Для добавления
        public string AddFieldValue(string value)
        {
            Console.WriteLine(value);
            return Console.ReadLine();
        }*/
        static void Main(string[] args)
        {
            using (PgDatabaseContext db = new PgDatabaseContext())
            {

                Customer Cus1 = new Customer { LastName = "Иванов", FirstName = "Иван", MiddleName = "Иванович", EMail = "us1@otus.ru" };
                Customer Cus2 = new Customer { LastName = "Петров", FirstName = "Петр", MiddleName = "Петрович", EMail = "us2@otus.ru" };
                Customer Cus3 = new Customer { LastName = "Соколов", FirstName = "Григорий", EMail = "us3@otus.ru" };
                Customer Cus4 = new Customer { LastName = "Михайлов", FirstName = "Михаил", MiddleName = "Михайлович", EMail = "us4@otus.ru" };
                Customer Cus5 = new Customer { LastName = "Сидоров", FirstName = "Владимир", MiddleName = "Петрович", EMail = "us5@otus.ru" };

                db.Customers.AddRange(Cus1, Cus2, Cus3, Cus4, Cus5);
                db.SaveChanges();
                
                Product Pr1 = new Product { Customer = Cus1, Name = "Дверь межкомнатная", Price = 3500, Description = "Продается межкомнатная деревянная дверь, новая, по вопросам писать в лс" };
                Product Pr2 = new Product { Customer = Cus1, Name = "Iphone 6s", Price = 5000, Description = "Продаю Iphone 6s, пользовался дедушка по выходным, есть мелкие царапины, полностью рабочий. Без торга." };
                Product Pr3 = new Product { Customer = Cus1, Name = "Принтер HP", Price = 1250, Description = "Продаю цветной принтер HP, на запчасти." };
                Product Pr4 = new Product { Customer = Cus4, Name = "Капот ВАЗ 2106", Price = 2640, Description = "Вмятин, сколов нет, бережно хранился." };
                Product Pr5 = new Product { Customer = Cus4, Name = "Кроссовки NIKE", Price = 3000, Description = "Беговые кроссовки Nike, новые, в коробке. Размер 44." };

                db.Products.AddRange(Pr1, Pr2, Pr3, Pr4, Pr5);
                db.SaveChanges();

                Favourite Fv1 = new Favourite { Customer = Cus1, Product = Pr1 };
                Favourite Fv2 = new Favourite { Customer = Cus1, Product = Pr3 };
                Favourite Fv3 = new Favourite { Customer = Cus2, Product = Pr5 };
                Favourite Fv4 = new Favourite { Customer = Cus4, Product = Pr3 };
                Favourite Fv5 = new Favourite { Customer = Cus5, Product = Pr2 };

                db.Favourites.AddRange(Fv1, Fv2, Fv3, Fv4, Fv5);
                db.SaveChanges();

                Console.WriteLine("Добро пожаловать! Данные уже в таблицах, нажми нужную кнопку.");
                while (true)
                {
                    var Customers = db.Customers.ToList();
                    var Products = db.Products.ToList();
                    var Favourites = db.Favourites.ToList();

                    Console.WriteLine("C - Посмотреть таблицу клиентов");
                    Console.WriteLine("P - Посмотреть таблицу товаров");
                    Console.WriteLine("F - Посмотреть таблицу избранного");
                    Console.WriteLine("=================================================");
                    Console.WriteLine("1 - Добавить клиента");
                    Console.WriteLine("2 - Добавить товар");
                    Console.WriteLine("3 - Добавить избранное");
                    Console.WriteLine("=================================================");
                    Console.WriteLine("X - ВЫХОД");
                    Console.WriteLine(">: ");
                    string k = Console.ReadLine().ToUpper();

                    if (k == "C")
                    {
                        Console.WriteLine("Клиенты:");
                        foreach (var Customer in Customers)
                        {
                            Console.WriteLine($"    ИД: {Customer.Id} | Фамилия: {Customer.LastName} | Имя: {Customer.FirstName} | Отчество: {Customer.MiddleName} | Эл.почта: {Customer.EMail}");
                        }
                    }
                    else if (k == "P")
                    {
                        Console.WriteLine("Товары");
                        foreach (var Product in Products)
                        {
                            Console.WriteLine($"    ИД: {Product.Id} | ИД Клиента: {Product.CustomerId} | Наименование: {Product.Name} | Цена: {Product.Price} | Валюта: {Product.Currency} | Описание: {Product.Description} | Дата публикации: {Product.DatePublish} | Объявление активно: {Product.IsActive}");
                        }
                    }
                    else if (k == "F")
                    {
                        Console.WriteLine("Избранное");
                        foreach (var Favourite in Favourites)
                        {
                            Console.WriteLine($"    ИД: {Favourite.Id} | ИД Клиента: {Favourite.CustomerId} | ИД Товара: {Favourite.ProductId} | Дата добавления: {Favourite.DateInsert} | Актуально: {Favourite.IsActive}");
                        }
                    }
                    else if (k == "1")
                    {
                        Console.WriteLine("Добавление клиента");
                        var clnt = new Customer();
                        clnt.LastName = AddFieldValue<string>("Введите фамилию");
                        clnt.FirstName = AddFieldValue<string>("Введите имя");
                        clnt.MiddleName = AddFieldValue<string>("Введите отчество");
                        clnt.EMail = AddFieldValue<string>("Введите адре эл.почты");

                        Console.WriteLine(DataWorker.AddCustomer(db, clnt));
                    }
                    else if (k == "2")
                    {
                        Console.WriteLine("Добавление товара");
                        var pr = new Product();
                        pr.CustomerId = AddFieldValue<int>("Введите ИД клиента");
                        pr.Name = AddFieldValue<string>("Введите наименование товара");
                        pr.Price = AddFieldValue<decimal>("Введите цену товара");
                        pr.Currency = AddFieldValue<string>("Введите валюту товара");
                        pr.Description = AddFieldValue<string>("Введите описание товара");

                        Console.WriteLine(DataWorker.AddProduct(db, pr));
                    }
                    else if (k == "3")
                    {
                        Console.WriteLine("Добавление избранного");
                        var fv = new Favourite();
                        fv.CustomerId = AddFieldValue<int>("Введите ИД клиента");
                        fv.ProductId = AddFieldValue<int>("Введите ИД продукта");

                        Console.WriteLine(DataWorker.AddFavourite(db, fv));
                    }
                    else if (k == "X")
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Введено неверное значение");
                        continue;
                    }
                }
            }
        }

        private static T AddFieldValue<T>(string description)
        {
            while (true)
            {
                try
                {

                    Console.WriteLine(description);
                    var value = Console.ReadLine();
                    var converter = TypeDescriptor.GetConverter(typeof(T));
                    if (converter != null)
                    {
                        return (T)converter.ConvertFromString(value);
                    }
                    return default(T);

                }
                catch (ArgumentException)
                {
                    Console.WriteLine("Введен неверный тип значения! Попробуйте снова.");
                }
                catch (NotSupportedException)
                {
                    return default(T);
                }
            }
        }
    }
}
