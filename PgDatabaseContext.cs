﻿
using ConnectDB.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace ConnectDB
{
    public class PgDatabaseContext : DbContext
    {

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Favourite> Favourites { get; set; }

        public PgDatabaseContext()
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=AvitoDb;Username=postgres;Password=0000");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().HasIndex(i => i.EMail).IsUnique();
        }
    }
}
