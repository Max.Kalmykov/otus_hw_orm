﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConnectDB.Entities
{
    /// <summary>
    /// Товары
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Идентификатор товара
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Наименование товара
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Цена товара
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// Валюта цены товара
        /// </summary>
        public string Currency { get; set; } = "RUB";
        /// <summary>
        /// Описание товара
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Дата публикации товара
        /// </summary>
        public DateTime DatePublish { get; set; } = DateTime.Now;
        /// <summary>
        /// Признак актуальности товара
        /// </summary>
        public bool IsActive { get; set; } = true;
        /// <summary>
        /// Внешний ключ на таблицу клиентов
        /// </summary>
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
    }
}
