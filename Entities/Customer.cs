﻿
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace ConnectDB.Entities
{
    /// <summary>
    /// Клиент
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// Идентификатор клиента
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Фамилия клиента
        /// </summary>
        [Required]
        public string LastName { get; set; }
        /// <summary>
        /// Имя клиента
        /// </summary>
        [Required]
        public string FirstName { get; set; }
        /// <summary>
        /// Отчество клиента
        /// </summary>
        public string? MiddleName { get; set; }
        /// <summary>
        /// Эл. почта
        /// </summary>
        [Required]
        [MinLength(6)]
        [MaxLength(100)]
        public string EMail { get; set; }
        
    }
}
