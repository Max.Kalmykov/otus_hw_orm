﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ConnectDB.Entities
{
    /// <summary>
    /// Избранное
    /// </summary>
    public class Favourite
    {
        /// <summary>
        /// Идентификатор избранного
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Дата добавления в избранное
        /// </summary>
        [Required]
        public DateTime DateInsert { get; set; } = DateTime.Now;
        /// <summary>
        /// Признак активности избранного
        /// </summary>
        public bool IsActive { get; set; } = true;
        /// <summary>
        /// Внешник ключ к таблице клиентов
        /// </summary>
        [Required]
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
        /// <summary>
        /// Внешник ключ к таблице товаров
        /// </summary>
        [Required]
        public int ProductId { get; set; }
        public Product Product { get; set; }

    }
}
