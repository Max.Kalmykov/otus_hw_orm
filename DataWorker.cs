﻿using ConnectDB.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConnectDB
{
    public class DataWorker
    {

        //Добавить клиента
        public static string AddCustomer(PgDatabaseContext context, Customer customer)
        {
            string res = "Клиент уже существует!";
            bool checkClient = context.Customers.Any(el => el.EMail == customer.EMail);
            if (!checkClient)
            {
                context.Customers.Add(customer);
                context.SaveChanges();
                res = "Клиент успешно добавлен!";
            }
       
            return res;
        }

        //Добавить товар
        public static string AddProduct(PgDatabaseContext context, Product product)
        {
            string res = "Товар уже существует!";
            bool check = context.Customers.Any(el => el.Id == product.CustomerId);
            if (check)
            {
                context.Products.Add(product);
                context.SaveChanges();
                res = "Товар успешно добавлен!";
            }
            else
            {
                res = "Клиента с указанным ИД не существует!";
            }

            return res;
        }

        //Добавить в избранное
        public static string AddFavourite(PgDatabaseContext context, Favourite favourite)
        {
            string res = "Товар успешно добавлен в избранное!";
            bool checkCustomer = context.Customers.Any(el => el.Id == favourite.CustomerId);
            bool checkProduct = context.Products.Any(el => el.Id == favourite.ProductId);
            bool checkUnique = context.Favourites.Any(el => el.CustomerId == favourite.CustomerId && el.ProductId == favourite.ProductId);

            if (!checkCustomer)
            {
                res = "Клиента с указнным ИД не существует!";
            }
            else if (!checkProduct)
            {
                res = "Товара с указанным ИД не существует!";
            }
            else if (checkUnique)
            {
                res = "Данный товар уже добавлен в избранное ранее!";
            }
            else
            {
                context.Favourites.Add(favourite);
                context.SaveChanges();
            }

            return res;
        }
    }
}
